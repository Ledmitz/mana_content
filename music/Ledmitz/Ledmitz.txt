Author = Ledmitz
License = CC-by-SA 3.0

Created for The Mana World / Evol Online / rEvolt

Genesis.ogg contains a sample from SkyRim(online game). Although the license matches, the warrior sounding "Aeeuuurugh!" is not fitting, in my opinion.
I may remake this one with different samples.

Genesis-old is almost entirely a premade bell sound loop that I added a lot of effects to. It was more for ambience, than music, but may be better used for sampling.

DrearyDay and OverwhelmedUnderground are both in their original state before looping and other edits. Might be nice to keep the originals around for something. I may add these
to the jukebox on rEvolt as extra songs... maybe easter eggs to find for Hocus' jukebox quest.

minigame-01 was created to sound like an 8-16 bit track to accompany Gumi's board game for Evol/rEvolt. AFAIK, it was never added. It was intentionally, overly happy sounding.
All sounds were generated/synthesized to create this sound.


